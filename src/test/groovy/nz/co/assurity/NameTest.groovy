package nz.co.assurity

import spock.lang.*

@Unroll
class NameTest extends spock.lang.Specification {
    def "#name's name is #length characters long"() {
        expect:
        name.size() == length

        where:
        name     | length
        "Spock"  | 5
        "Kirk"   | 4 
        "Scotty" | 6
    }
}  
